const { PaymentCreator, listPayment } = require("./model")

const createPayment = (data) => {
    const { payed, productId } = data;

    if(!canBuy(payed, productId)) {
        throw new Error("Cannot buy")
    }
    const Payment = PaymentCreator(data)
    bought(productId)
    return Payment
}

const getPayments = () => {
    return listPayment()
}

const getPayment = (id) => {
    return listPayment(id)
}

module.exports = {
    createPayment,
    getPayments,
    getPayment
}