const is = require('is_js')
const { v4: uuidv4 } = require('uuid');
const { listProduct } = require('../product/model');
const { listUser } = require('../user/model')

const payments = [];

const paymentCreator = (data) => {
    const { buyerId, productId } = data;

    if (!buyerId || !productId) {
        throw new Error("No id specified")
    }

    if (is.empty(listUser(buyerId)) || is.empty(listProduct(productId))) {
        throw new Error("Buyer or product does not exists")
    }

    const newpayment = {
        id: uuidv4(),
        date: Date.now(),
        ...data
    };

    Object.freeze(newpayment);

    payments.push(newpayment);

    return newpayment;
}

const listPayment = (id) => {
    if (id) {
        const payment = payments.find(({ id: _id }) => id === _id)
        if (!payment) {
            throw new Error('payment not found')
        }
        return payment;
    }
    return payments;
}

const updatePayment = (paymentId, newData) => {
    const paymentPosition = payments.findIndex(({ id }) => id == paymentId);
    payments.splice(paymentPosition, 1)
    paymentCreator(newData)
}

module.exports = {
    paymentCreator,
    listPayment,
    updatePayment
}