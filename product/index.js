const { productCreator, listProduct } = require("./model")

const createProduct = (data) => {
    const products = productCreator(data)
    return products
}

const getProducts = () => {
    return listProduct()
}

const getProduct = (id) => {
    return listProduct(id)
}

module.exports = {
    createProduct,
    getProducts,
    getProduct
}