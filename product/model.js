const is = require('is_js')
const { v4: uuidv4 } = require('uuid');
const { listUser } = require('../user/model')

const products = [];

const productCreator = (data) => {
    const { name, price, userId, quantity } = data;

    // if (is.not.alphaNumeric(name) || is.not.number(price) || is.not.number(quantity) || is.empty(listUser(userId))) {
    //     throw new Error("Wrong data for product")
    // }
    if (quantity < 0 || price < 0) {
        throw new Error("Quantity or price below 0 !")
    }

    const newProduct = {
        id: uuidv4(),
        ...data
    };

    Object.freeze(newProduct);

    products.push(newProduct);

    return newProduct;
}

const listProduct = (id) => {
    if (id) {
        const product = products.find(({ id: _id }) => id === _id)
        if (!product) {
            throw new Error('product not found')
        }
        return product;
    }
    return products;
}

const updateProduct = (productId, newData) => {
    const productPosition = products.findIndex(({ id }) => id == productId);
    products.splice(productPosition, 1)
    productCreator(newData)
}

const canBuy = (amount, productId) => {
    const product = products.find(({ id }) => id === productId);

    if (!product || product.price > amount) {
        return false
    }

    return true;
}

const bought = (productId) => {
    const product = products.find(({ id }) => id === productId);
    product.quantity--;

    const productPosition = products.findIndex(({ id }) => id == productId);
    products.splice(productPosition, 1)
    products.push(product)
}

module.exports = {
    productCreator,
    listProduct,
    updateProduct,
    canBuy
}