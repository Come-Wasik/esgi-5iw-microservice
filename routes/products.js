var express = require('express');
const { createProduct, getProducts, getProduct } = require('../product');
var router = express.Router();

/* GET product listing. */
router.get('/', function (req, res, next) {
  const product = getProducts();
  res.send(product);
});

router.post('/', function (req, res) {
  const data = req.body;

  try {
    const newProduct = createProduct(data);
    res.send(newProduct);
  } catch (error) {
    res.send({ error: error.message });
  }
})

router.get('/:id', function (req, res, next) {
  const { id } = req.params;
  try {
    const product = getProduct(id)
    res.send(product)
  } catch(error) {
    res.send({ error: error.message });
  }
});

module.exports = router;
