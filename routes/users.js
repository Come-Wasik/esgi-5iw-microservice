var express = require('express');
const { createUser, getUsers, getUser } = require('../user');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  const users = getUsers();
  res.send(users);
});

router.post('/', function (req, res) {
  const data = req.body;

  try {
    const newUser = createUser(data);
    res.send(newUser);
  } catch (error) {
    res.send({ error: error.message });
  }
})

router.get('/:id', function (req, res, next) {
  const { id } = req.params;
  try {
    const user = getUser(id)
    res.send(user)
  } catch(error) {
    res.send({ error: error.message });
  }
});

module.exports = router;
