const is = require('is_js')
const { v4: uuidv4 } = require('uuid');

const users = [];

const userCreator = (data) => {
    const { name, email, password } = data;

    if (is.not.alphaNumeric(name)) {
        throw new Error("No name provided")
    }
    if (is.not.email(email)) {
        throw new Error("No email provided")
    }
    if (is.empty(password)) {
        throw new Error("No password provided")
    }

    const newUser = {
        id: uuidv4(),
        ...data
    };

    Object.freeze(newUser);

    users.push(newUser);

    return newUser;
}

const listUser = (id) => {
    if (id) {
        const user = users.find(({ id: _id }) => id === _id)
        if (!user) {
            throw new Error('User not found')
        }
        return user;
    }
    return users;
}

module.exports = {
    userCreator,
    listUser
}